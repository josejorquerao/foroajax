@extends('layouts.template')
@section('seccion')
<div>
    <h5>Mis favoritos: </h5>
</div>
<br> 
@if(count($fav)>0)
  @foreach($fav as $favs)
  <div class="card" id="list-post">
    <div class="card-body" id="tablapost">
      <div class="mb-2">
        <a class="h5" style="color:#02709d" href="#">{{$favs}}</a>
      </div>
    </div>
  </div>
  <br>
  @endforeach
@endif
@endsection