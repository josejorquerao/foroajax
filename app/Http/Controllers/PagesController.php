<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Category;
use App\Post;
use App\User;
use App\Comment;
use DB;
use URL;

class PagesController extends Controller
{
    public function allpost(Request $request)
    {
        $id= $request->input('category_id');
        $allpost = Post::
        select('title','content')
        ->where('category_id','=',$id)
        ->get();
        return view ('list-post')-with('posts',$allpost); 

    }
    public function index()
    {
        $categoria=Category::all();
        return view('index', compact('categoria'));
    }

    //Listar
    public static function posts($id)
    {
        $posts=Category::find($id)->posts;
        $cat=Category::find($id);
        return view('posts', compact('posts','cat'))->with('url',URL::to('/'));    
    }
    
    public function posteo($id)
    {
        $post=Post::find($id);
        return view('post', compact('post'));      
    }

    //Insertar
    public function insertarPost(Request $request)
    {
        if ($request->ajax()){
        $crearPost = Post::create([
            'title'=>$request->get('title'),
            'content'=>$request->get('content'),
            'category_id'=>$request->get('category_id'),
            'users_id'=>auth()->user()->id,
        ]);
        return response()->json(['post'=> $crearPost]);
        }
    }
    public function insertarCmt(Request $request)
    {
        $crearCmt = Comment::create([
            'comment'=>$request->get('comment'),
            'post_id'=>$request->get('posteo'),
            'users_id'=>auth()->user()->id,
        ]);
        return back()->with('mensaje','Comentario creado.');  
    }
    public function insertarCat(Request $request)
    {
        $crearCat = Category::create([
            'name'=>$request->get('name'),
            'description'=>$request->get('description'),
        ]);
        return back()->with('mensaje','Categoria creada.');  
    }

    //Eliminar
    public function borrarCmt($id)
    {
        $borrarCmt = Comment::find($id);
        $borrarCmt->delete();
        return back()->with('mensaje','Comentario eliminado.'); 

    }
    public function borrarPost($id)
    {
        $borrarPost = Post::find($id);
        $borrarPost->delete();
        return back()->with('mensaje','Publicacion eliminada.'); 

    }
    public function borrarCat($id)
    {
        $borrarCat = Category::find($id);
        $borrarCat->delete();
        return back()->with('mensaje','Categoria eliminada.'); 

    }

    //Editar
    public function editarCat(Request $request, $id)
    {
        $editarCat=Category::find($id);
        $editarCat->name=$request->get('name');
        $editarCat->description=$request->get('description');
        $editarCat->save();
        return back()->with('mensaje','Categoria editada.'); 

    }
    public function editarPost(Request $request, $id)
    {
        $editarPost=Post::find($id);
        $editarPost->title=$request->get('title');
        $editarPost->content=$request->get('content');
        $editarPost->save();
        return back()->with('mensaje','Publicacion editada.'); 

    }
    public function editarCmt(Request $request, $id)
    {
        $editarCmt=Comment::find($id);
        $editarCmt->comment=$request->get('comment');
        $editarCmt->save();
        return back()->with('mensaje','Comentario editado.'); 

    }
    public function listPost(){
        $post = Post::
                    select('title')
                    ->get();
        return View('list-post')->with('posts',$post);

    }
}
?>